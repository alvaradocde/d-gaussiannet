import torch
from models.dgaussiannet import DGaussianNet

# dummy example
dummy = torch.ones((32, 1, 224, 224))
model = DGaussianNet(1)
print(model(dummy).shape)
print(model)
