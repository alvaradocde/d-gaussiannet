# D-GaussianNet

This repository contains the implementation of the convolutional neural architecture **D-GaussianNet**, described in the paper [D-GaussianNet: Adaptive Distorted Gaussian Matched Filter with Convolutional Neural Network for Retinal Vessel Segmentation](https://doi.org/10.1007/978-3-030-72073-5_29).


## Citation

If the code is helpful for your research, please consider citing:

  ```shell
  @article{alvarado2021d,
    title={D-GaussianNet: Adaptive Distorted Gaussian Matched Filter with Convolutional Neural Network for Retinal Vessel Segmentation},
    author={Alvarado-Carrillo, Dora E and Ovalle-Magallanes, Emmanuel and Dalmau-Cede{\~n}o, Oscar S},
    journal={Geometry and Vision},
    volume={1386},
    pages={378},
    year={2021},
    publisher={Nature Publishing Group}
  }
  ```
